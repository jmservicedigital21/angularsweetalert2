import { Component } from '@angular/core';
import { Clipboard } from '@angular/cdk/clipboard';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'angularSweetAlert2';
  animateCSScdn =
    'https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css';
  couponCode = 'NGFIRE6HOME';
  message = '';

  constructor(private clipboard: Clipboard) {}

  copy() {
    this.clipboard.copy(this.message);
    console.log('data copied is :', this.message);
  }
  onValueInput(e) {
    this.message = e.target.value;
  }
}
